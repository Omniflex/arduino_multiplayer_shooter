[CONCEPT]
This is the code for a mutliplayer arduino game that looks like a shooting gallery.
Each player takes control over a turret and must kill the maximum number of enemies
they can before the timer runs out.

[GAMEPLAY]
After a short calibration session where each player has to shoot at crosses at the edges
of the screen to take into account that the 'controllers' can move, the game begins.
They are three types of killable entities in the game :
-Jellyfishes that stay longer on the screen but grant 1 point
-Aliens that stay for less time but give 2 points
-Humans that stay for a bit longer but take away 3 points
Each player uses their turret to kill enemies quickly and gain lots of points
When the timer reaches 0, the player having the highest score wins. A draw is possible.

[IMPLEMENTATION]
This game needs two buttons, two potentiometers and a 16x2 LCD screen, although these dimensions can
be changed in the code. Arduino's LiquidCrystal library is used, and the wiring follows
the first LCD example shown in the Arduino UNO beginner guide. Don't forget the pull-up
resistors and leave analog pin 0 unwired in order to use a different random seed each time.
I cut out two shapes of cardboard that look like clock hands and stuck them to the potentiometers
in order to make each player's line of sight clearer. The 'remote controle' part
of the hardware was put on a different breadboard to make it more playable. But be careful,
as moving these sights or moving the controllers during gameplay will surelly mess with the algorithm, as 
everything is designed to work with the values obtained from the calibration phase.

[IMPROVEMENTS]
A clean refactor would be a good idea : putting every function in a separate file could
render the .ino more readable. Adding some power-ups, like having a laser sight
or a blast effect on the adjacent tiles would be awesome!

[LICENSE]
This project is under GNU GPL, so feel free to use it as you want!

[CONTACT]
omniflex@outlook.fr
