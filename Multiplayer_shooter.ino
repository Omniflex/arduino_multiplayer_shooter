#include <LiquidCrystal.h>
LiquidCrystal lcd(12,11,5,4,3,2);

int board[16][2];
int prev_board[16];
const int framerate = 60;
const float period = (1000 / framerate);
const float cooldown = 0.5;
const int meduse_time = 4;
const int alien_time = 2;
const int civil_time = 3;
unsigned long time_prev = millis();
bool GO = false;
int refresh = 60;
int prev_scorep1 = 0;
int prev_scorep2 = 0;
int scorep1 = 0;
int scorep2 = 0;
int game = 0;
int enemy_count = 0;
int max_enemy = 6;
int play_time = 40;
int frame = 0;
int count_time = 0;
int prev_count_time = 0;
bool cheat = false;


int p1 = A3;
int p2 = A2;
int b1 = 8;
int b2 = 9;

int calibration = 0;
int LR = 0;
bool printed = false;
bool press1 = false;
bool prev1 = false;
bool press2 = false;
bool prev2 = false ;

int aim1 = 0;
int aim2 = 0;
int hit1 = 0;
int hit2 = 0;

int h1 = 1023;
int h2 = 1023;
int l1 = 0;
int l2 = 0;

int v1 = 16;
int v2 = 16;

int cool1 = 0;
int cool2 = 0;

byte meduse[8] = {
  0b01110,
  0b10001,
  0b10001,
  0b01110,
  0b11111,
  0b10101,
  0b10101,
  0b10101
};

byte alien[8] = {
  0b10001,
  0b01110,
  0b01010,
  0b01110,
  0b01110,
  0b10101,
  0b01110,
  0b01010
};

byte civil[8] = {
  0b01110,
  0b01010,
  0b01110,
  0b00100,
  0b01110,
  0b10101,
  0b01110,
  0b01010
};

byte shot[8] = {
  0b00100,
  0b10101,
  0b01110,
  0b11111,
  0b01110,
  0b10101,
  0b00100,
  0b00000
};


void init_prompt(){
  lcd.setCursor(2,0);
  lcd.print("Shoot'em up!");

  lcd.setCursor(0,1);
  lcd.write((uint8_t)(1));
  lcd.print("=1");

  lcd.setCursor(6,1);
  lcd.write((uint8_t)(2));
  lcd.print("=2");

  lcd.setCursor(12,1);
  lcd.write((uint8_t)(3));
  lcd.print("=-3");

  delay(4000);
  lcd.clear();
  game = 2;
  lcd.setCursor(0,1);
  lcd.print("0");
  lcd.setCursor(15,1);
  lcd.print("0");
  lcd.setCursor(7,1);
  lcd.print(play_time);
}

void reset(){
  //Init random seed
  randomSeed(analogRead(0));

  //Init board array
  for (int i=0;i<15;i++) {
    prev_board[i] = 0;
    board[i][0] = 0;
    board[i][1] = 0;
  }

  press1 = false;
  prev1 = false;
  press2 = false;
  prev2 = false ;
  prev_scorep1 = 0;
  prev_scorep2 = 0;
  scorep1 = 0;
  scorep2 = 0;
  game = 1;
  enemy_count = 0;
  frame = 0;
  count_time = 0;
}

void timer(){
  if (frame == framerate){
    frame = 0;
    count_time++;
    if(count_time == play_time){
      GO = true;
      lcd.setCursor(0,0);
      lcd.print("                ");
      lcd.setCursor(7,1);
      lcd.print("  ");
      lcd.setCursor(4,0);
      if (scorep1>scorep2){
        lcd.print("P1 wins !");
      }
      else if (scorep1<scorep2){
        lcd.print("P2 wins !");
      }
      else if (scorep1==scorep2){
        lcd.setCursor(6,0);
        lcd.print("Draw !");
      }
      delay(2000);
      lcd.clear();
      reset();
    }
  }
  else{
  frame++;
  }
}

void draw_board(){
  for (int i=0;i<16;i++){
    if (prev_board[i] != board[i][0]){
      lcd.setCursor(i, 0);
      if(board[i][0]!=0){
        lcd.write((uint8_t)(abs(board[i][0])));
      }
      else{
        lcd.print(" ");
      }
    }
  }
  if (prev_scorep1 != scorep1){
    lcd.setCursor(0,1);
    lcd.print("    ");
    lcd.setCursor(0,1);
    lcd.print(scorep1);
  }
  if(prev_scorep2 != scorep2){
    lcd.setCursor(12,1);
    lcd.print("    ");
    lcd.setCursor(15,1);
    lcd.print(scorep2%10);
    if (scorep2 >= 10){
      lcd.setCursor(14,1);
      lcd.print((scorep2/10)%10);
    }
    if (scorep2 >= 100){
      lcd.setCursor(13,1);
      lcd.print((scorep2/100)%10);
    }
  }
  if(prev_count_time != count_time){
    lcd.setCursor(7,1);
    lcd.print("  ");
    lcd.setCursor(8,1);
    lcd.print((play_time-count_time)%10);
    if((play_time-count_time) >= 10){
      lcd.setCursor(7,1);
      lcd.print(((play_time-count_time)/10)%10);
    }
    else{
      lcd.setCursor(7,1);
      lcd.print("0");
    }
  }
}


void collision(){

    if(press1 and cool1 == 0 and not prev1){
      prev1 = true;
      if (v1 >= 0 and v1 < 16 and ((board[v1][0] > 0 and board[v1][0] < 4) or board[v1][0] == -3)){
        scorep1 = scorep1 + board[v1][0];
        if(scorep1<0){
          scorep1=0;
        }
        enemy_count--;
      }
      if (v1 >= 0 and v1 < 16){
        board[v1][0] = 4;
        board[v1][1] = cooldown*framerate;
      }
      cool1 = framerate * cooldown;
    }

    if(press2 and cool2 == 0 and not prev2){
      prev2 = true;
      if (v2 >= 0 and v1 < 16 and ((board[v2][0] > 0 and board[v2][0] < 4) or board[v2][0] == -3)){
        scorep2 = scorep2 + board[v2][0];
        if(scorep2<0){
          scorep2=0;
        }
        enemy_count--;
      }
      if (v2 >= 0 and v2 < 16){
        board[v2][0] = 4;
        board[v2][1] = cooldown*framerate;
      }
      cool2 = framerate*cooldown;
    }

}

void manage_enemy(){
  //Despawn enemies and gunshots
  for (int i=0;i<16;i++){
    if (board[i][0] > 0 or board[i][0] <=  4 or board[i][0] == -3){
      --board[i][1];
      if (board[i][1] == 0){
         board[i][0] = 0;
         --enemy_count;
      }
    }
  }

  //Spawn enemies
  if (enemy_count < max_enemy){
    int spawn = random(60);
    int placement = random(16);

    if (board[placement][0] == 0){
      switch (spawn){
        case 1 : board[placement][0] = 1; board[placement][1] = meduse_time * framerate; ++enemy_count; break;
        case 2 : board[placement][0] = 2; board[placement][1] = alien_time * framerate; ++enemy_count; break;
        case 3 : board[placement][0] = -3; board[placement][1] = civil_time * framerate; ++enemy_count; break;
      }
    }
  }
}


void print_calib(){
  //Print prompt
  if (printed == false){
    switch (calibration){
      case 0 : {
        lcd.setCursor(0,1);
        lcd.print("Calibration P1");

        switch (LR){
          case 0 :{
            lcd.setCursor(0,0);
            lcd.print("x");
            break;
          }
          case 1 :{
            lcd.setCursor(15,0);
            lcd.print("x");
            break;
          }
        }
      break;
      }

      case 1 : {
        lcd.setCursor(0,1);
        lcd.print("Calibration P2");

        switch (LR){
          case 0 :{
            lcd.setCursor(0,0);
            lcd.print("x");
            break;
          }
          case 1 :{
            lcd.setCursor(15,0);
            lcd.print("x");
            break;
          }
        }
      break;
      }
    }
  }
}

void gun_cooler(){
  if (cool1 > 0){
    --cool1;
    }

  if (cool2 > 0){
    --cool2;
    }
}


void scope(){
  v1 = map(aim1,l1,h1,15,0);
  v2 = map(aim2,l2,h2,15,0);
}


void calibrate(){
  print_calib();

    if (calibration ==  0){
      if (press1){
        switch (LR){
          case 0 : h1 = analogRead(p1); LR = 1; lcd.clear(); delay(500); break;
          case 1 : l1 = analogRead(p1); LR = 0; calibration = 1; lcd.clear(); break;
        }
      }
    }

    if (calibration ==  1){
      if (press2){
        switch (LR){
          case 0 : h2 = analogRead(p2); LR = 1; lcd.clear(); delay(500); break;
          case 1 : l2 = analogRead(p2); LR = 0; delay(500); calibration = 2; lcd.clear(); game = 1; break;
        }
      }
    }

}


void get_aim(){
  aim1 = analogRead(p1);
  aim2 = analogRead(p2);
}


void check_press(){
    if (digitalRead(b1) == HIGH){
    press1 = true;
  }
  else {
    press1 = false;
    prev1 = false;
  }


  if (digitalRead(b2) == HIGH){
    press2 = true;
  }
  else {
    press2 = false;
    prev2 = false;
  }
}






void setup() {
  //Init random seed
  randomSeed(analogRead(0));

  //Init board array
  for (int i=0;i<15;i++) {
    prev_board[i] = 0;
    board[i][0] = 0;
    board[i][1] = 0;
  }

  //Init LCD screen and buttons
  lcd.begin(16,2);
  pinMode(b1, INPUT);
  pinMode(b2, INPUT);
  pinMode(p1, INPUT);
  pinMode(p2, INPUT);

  //Init custom char
  lcd.createChar(1, meduse);
  lcd.createChar(2, alien);
  lcd.createChar(3, civil);
  lcd.createChar(4, shot);

  //Init Serial data transfer for debug
  Serial.begin(9600);
}

void loop() {

  unsigned long time_beg = millis();
//  Serial.print(1000/(time_beg-time_prev));
//  Serial.print("\n");
  time_prev = time_beg;

  //Checking for user input
  check_press();
  get_aim();

  //Debug serial print
//  Serial.print(String(digitalRead(b1)) + " : ");
//  Serial.print(String(digitalRead(b2)) + "\n");
//  Serial.print(String(analogRead(p1)) + " : ");
//  Serial.print(String(analogRead(p2)) + "\n\n");
//  Serial.print(String(l1) + " : " + String(h1) + "\n");
//  Serial.print(String(l2) + " : " + String(h2) + "\n");

  //Initial prompt
  if (game == 1){
    init_prompt();
  }
  //Calibration
  if (calibration != 2){
    calibrate();
  }

  if (game==2){

    for (int i=0;i<16;i++){
      prev_board[i] = board[i][0];
    }

    prev_scorep1 = scorep1;
    prev_scorep2 = scorep2;
    prev_count_time = count_time;

    gun_cooler();
    scope();
    manage_enemy();

    if (cheat){
      board[0][0] = 1;
      board[1][0] = 2;
      board[2][0] = -3;
    }
    collision();
    timer();
    if (not GO){
      draw_board();
    }

//  lcd.clear();
//  lcd.setCursor(v1,0);
//  lcd.print("X");
//  lcd.setCursor(v2,0);
//  lcd.print("Y");

  }


  if (GO == false and game == 2) {
    unsigned long time_end = millis();
    int time_frame = time_end - time_beg;
//    Serial.print(String(period - time_frame)+ "\n");
    if (period - time_frame > 0) {
      delay(period - time_frame);
    }
  }

  if(GO == true){
    GO = false;
  }
}
